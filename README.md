# JPEG codec in Rust

This is an experiment in writing a new JPEG codec in Rust with the
following features:

* Uses the SIMD functions from libjpeg-turbo.
* Has easy "encode me a JPEG" and "decode me a JPEG" APIs.
* Has detailed APIs to build custom encoding and decoding pipelines.

Eventually the idea is to wrap the Rust API in a C API that can be
used from language bindings, to hopefully provide a modern,
general-purpose JPEG library to the world.

