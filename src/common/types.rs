/// Type for the value of a single color channel.
///
/// We want to support 8-bit and 12-bit JPEG images, so the rest of the code
/// should be generic on samples that implement this trait.
///
/// This is equivalent to `JSAMPLE`.
pub trait Sample {
    /// Maximum value, plus 1, that a sample can attain = 2^BITS.
    const RANGE_UPPER: usize; // MAXJSAMPLE + 1

    /// Maximum value that a sample can attain = 2^BITS - 1.
    const MAX: i32; // MAXJSAMPLE

    const CENTER: i32; // CENTERJSAMPLE
    const BITS: i32; // BITS_IN_JSAMPLE

    /// Maximum value for an entry in a quantization table.
    const QUANT_TABLE_MAX_VALUE: u16;

    /// For the DefineQuantizationTables marker; 0 for 8-bit, 1 for 16-bit (12-bit JPEG).
    const QUANT_TABLE_PRECISION: u8;

    /// Gets the sample's value as an `i32`.
    fn get(&self) -> i32; // GETJSAMPLE()

    /// Casts an i32 to a sample value by truncation
    fn from(x: i32) -> Self;
}

/// Type for 8-bit samples.  Most JPEG images will use this.
pub type Sample8 = u8;

impl Sample for Sample8 {
    const RANGE_UPPER: usize = 256;
    const MAX: i32 = 255;
    const CENTER: i32 = 128;
    const BITS: i32 = 8;
    const QUANT_TABLE_MAX_VALUE: u16 = 255;
    const QUANT_TABLE_PRECISION: u8 = 0;

    fn get(&self) -> i32 {
        i32::from(*self)
    }

    fn from(x: i32) -> Self {
        x as u8
    }
}

/// Type for 12-bit samples.
// Note that libjpeg-turbo uses short for 12-bit samples, but prefers unsigned char for
// 8-bit samples.  I don't know if the signedness makes a difference.
pub type Sample12 = i16;

impl Sample for Sample12 {
    const RANGE_UPPER: usize = 4096;
    const MAX: i32 = 4095;
    const CENTER: i32 = 2048;
    const BITS: i32 = 12;
    const QUANT_TABLE_MAX_VALUE: u16 = 32767;
    const QUANT_TABLE_PRECISION: u8 = 1;

    fn get(&self) -> i32 {
        i32::from(*self)
    }

    fn from(x: i32) -> Self {
        x as i16
    }
}
