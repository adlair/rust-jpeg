use criterion::{black_box, criterion_group, criterion_main, Criterion};

use libc::c_long;

use jpeg_rust::common::color_convert::RgbYcc;
use jpeg_rust::common::types::{Sample, Sample8};

use mozjpeg_sys::{JCS_RGB, jnosimd_rgb_ycc_convert, jpeg_color_converter_input};

fn bench_rgb_ycc(c: &mut Criterion) {
    c.bench_function("rgb_ycc Sample8", |bencher| {
        let conv = RgbYcc::new();

        let x = vec![1u8; 3000];
        let mut a = vec![0u8; 1000];
        let mut b = vec![0u8; 1000];
        let mut c = vec![0u8; 1000];

        bencher.iter(|| conv.rgb_to_ycc(&x[..], &mut a[..], &mut b[..], &mut c[..]))
    });
}

fn fix(x: f32) -> c_long {
    // FIXME: use cast crate to avoid "as i32"
    (x * 65536.0 + 0.5) as c_long
}

macro_rules! subtable_size {
    () => {
        Sample8::RANGE_UPPER
    };
}

// copied from jccolor.c
const TABLE_SIZE: usize = (8 * subtable_size!());
const ONE_HALF: c_long = 1 << (16 - 1);
const CBCR_OFFSET: c_long = (Sample8::CENTER as c_long) << 16;

fn nosimd_rgb_ycc_table() -> Box<[c_long]> {
    let mut table = vec![0; TABLE_SIZE];

    {
        // Split the table mutably into sub-tables
        let (r_y, table) = table.split_at_mut(subtable_size!());
        let (g_y, table) = table.split_at_mut(subtable_size!());
        let (b_y, table) = table.split_at_mut(subtable_size!());

        let (r_cb, table) = table.split_at_mut(subtable_size!());
        let (g_cb, table) = table.split_at_mut(subtable_size!());
        let (b_cb, table) = table.split_at_mut(subtable_size!());

        // B=>Cb and R=>Cr tables are the same
        // let (r_cr, table) = table.split_at_mut(subtable_size!());
        let (g_cr, table) = table.split_at_mut(subtable_size!());
        let (b_cr, table) = table.split_at_mut(subtable_size!());

        for i in 0 .. subtable_size!() {
            let ii = i as c_long;

            r_y[i] = fix(0.29900) * ii;
            g_y[i] = fix(0.58700) * ii;
            b_y[i] = fix(0.11400) * ii + ONE_HALF;

                r_cb[i] = (-fix(0.16874)) * ii;
                g_cb[i] = (-fix(0.33126)) * ii;
                // We use a rounding fudge-factor of 0.5-epsilon for Cb and Cr.
                // This ensures that the maximum output will round to MAXJSAMPLE
                // not MAXJSAMPLE+1, and thus that we don't have to range-limit.
                b_cb[i] = fix(0.50000) * ii + CBCR_OFFSET + ONE_HALF - 1;

                // B=>Cb and R=>Cr tables are the same
                // r_cr[i] = fix(0.50000) * ii + CBCR_OFFSET + ONE_HALF - 1;

                g_cr[i] = (-fix(0.41869)) * ii;
                b_cr[i] = (-fix(0.08131)) * ii;
        }
    }

    table.into_boxed_slice()
}

fn bench_nosimd_rgb_ycc(c: &mut Criterion) {
    c.bench_function("nosimd_rgb_ycc Sample8", |bencher| {
        let table = nosimd_rgb_ycc_table();
        let table = &table[..];

        let x = vec![1u8; 3000];
        let mut a = vec![0u8; 1000];
        let mut b = vec![0u8; 1000];
        let mut c = vec![0u8; 1000];

        let x_samparray = [x.as_ptr()];

        let mut plane_a = [a.as_mut_ptr()];
        let mut plane_b = [b.as_mut_ptr()];
        let mut plane_c = [c.as_mut_ptr()];
        let mut output = [plane_a.as_mut_ptr(), plane_b.as_mut_ptr(), plane_c.as_mut_ptr()];

        let input = jpeg_color_converter_input {
            input_buf: x_samparray.as_ptr(),
            image_width: 1000,
            in_color_space: JCS_RGB,
            input_components: 3,
            num_components: 3,
            rgb_ycc_tab: table.as_ptr(),
        };

        bencher.iter(|| unsafe {
            jnosimd_rgb_ycc_convert(
                &input,
                output.as_mut_ptr(),
                0,
                1,
            )
        })
    });
    
}

criterion_group!(benches, bench_rgb_ycc, bench_nosimd_rgb_ycc);
criterion_main!(benches);
